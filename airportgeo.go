package airportgeo

import (
	"encoding/csv"
	"fmt"
	"github.com/kellydunn/golang-geo"
	"io"
	"os"
	"strconv"
)

var listAirport []*geo.Point
var listName []string
var filename string = "airport.csv"

func init() {
	getallairport()
}

func FindNearAirport(searchpointLat, searchpointLng float64) (distance float64, lat float64, lng float64, code string) {
	searchpoint := geo.NewPoint(searchpointLat, searchpointLng)
	distance = 10000
	for k, airport := range listAirport {
		dist := searchpoint.GreatCircleDistance(airport)
		if distance > dist {
			distance = dist
			lat = airport.Lat()
			lng = airport.Lng()
			code = listName[k]
		}
	}
	return distance, lat, lng, code
}

func getallairport() {
	var tmplist []*geo.Point
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer file.Close()
	reader := csv.NewReader(file)
	reader.Comma = ','

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("Error:", err)
			return
		}
		code := record[4]
		if code != "" {
			fl1, _ := strconv.ParseFloat(record[6], 64)
			fl2, _ := strconv.ParseFloat(record[7], 64)
			tmplist = append(tmplist, geo.NewPoint(fl1, fl2))
			listName = append(listName, code)
		}
	}
	listAirport = tmplist
}
