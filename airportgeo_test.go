package airportgeo_test

import (
	"airportgeo"
	"fmt"
	"testing"
	"time"
)

func Test_FindNearAirport(T *testing.T) {
	findAndShowLog(42.25, 120.2)           // chiny
	findAndShowLog(51.1270779, 16.9918639) // polska - wroclaw
	findAndShowLog(47.146512, 11.195897)   // austria
	findAndShowLog(46.425505, 98.828518)   // mongolia
	findAndShowLog(45.542222, 9.203333)    // MILANO
}

func findAndShowLog(lat, lng float64) {
	starttime := time.Now()
	distance, lat, lng, code := airportgeo.FindNearAirport(lat, lng)
	fmt.Printf("Near aiport: %s\tgeo(%f, %f)\tdistance: %f, find in time: %v\n", code, lat, lng, distance, time.Now().Sub(starttime))
}

func BenchmarkFunction(b *testing.B) {
	fmt.Println(b.N)
	for i := 0; i < b.N; i++ {
		airportgeo.FindNearAirport(51.1270779, 16.9918639)
	}
}
